const express = require('express');
const router = express.Router();

router.get('/:id', function (req, res) {
  res.status(200);
});

router.put('/:id', function (req, res) {
  res.status(201);
});

router.post('/', function (req, res) {
  res.status(201);
});

module.exports = router;

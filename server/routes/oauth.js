const app = require('../app');
const config = require('config');
const passport = require('passport');
const OAuthStrategy = require('passport-oauth').OAuthStrategy;

app.get('/provider', passport.authenticate('provider', {scope: ['email', 'name']}));

passport.use('provider',
  new OAuthStrategy({
    requestTokenURL: config.auth.requestTokenURL,
    accessTokenURL: config.auth.accessTokenURL,
    userAuthorizationURL: config.auth.userAuthorizationURL,
    consumerKey: config.auth.consumerKey,
    consumerSecret: config.auth.consumerSecret,
    callbackURL: config.auth.callbackURL
  }, function (token, tokenSecret, profile, done) {
    // TODO: create auth function
    //   User.findOrCreate(..., function(err, user) {
    //       done(err, user);
    //   });
  })
);

const mongoose = require('mongoose');
const config = require('../config');
mongoose.connect(`mongodb://${config.db.user}:${config.db.pass}@${config.db.host}/${config.db.db}`);

module.exports.default = mongoose;
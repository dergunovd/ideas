const mongoose = require('./index');
const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  github: {
    type: String,
    required: true
  }
});
const userModel = mongoose.model('User', userSchema);

module.exports = userModel;

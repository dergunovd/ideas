const mongoose = require('./index');
const ideaSchema = mongoose.Schema({
  name: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  author: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'User',
    required: true
  },
  assign: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'User',
  },
  description: {
    type: String,
    required: true,
    default: ''
  },
  date: {
    type: Date,
    default: Date.now,
    required: true
  }
});
const ideaModel = mongoose.model('Idea', ideaSchema);

module.exports = ideaModel;

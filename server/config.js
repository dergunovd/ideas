module.exports = {
  auth: {
    requestTokenURL: process.env.AUTH_REQUEST_URL || '',
    accessTokenURL: process.env.AUTH_ACCESS_URL || '',
    userAuthorizationURL: process.env.AUTH_AUTH_URL || '',
    consumerKey: process.env.AUTH_CONSUMER_KEY || '',
    consumerSecret: process.env.AUTH_CONSUMER_SECRET || '',
    callbackURL: process.env.AUTH_CALLBACK_URL || ''
  },
  ssl: {
    key: '',
    cert: '',
  },
  db: {
    host: process.env.DB_HOST || '',
    user: process.env.DB_USER || '',
    pass: process.env.DB_PASS || '',
    db: process.env.DB_DB || '',
  }
};
